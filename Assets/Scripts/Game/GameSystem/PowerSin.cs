﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PowerSin  {

	
	// Sins : {Envy, Pride, Rage, Lust, Vanity, Lazy, Greed}
	
	public static List<string> SELECTEDSINS;
	public static List<string> SINS;
	public HistoryHolder.Sins Sin;
	public HistoryHolder.Sins touchedSin;
	public string sinString;
	public string Virtue1 = "";
	public string Virtue2 = "";
	
	public  PowerSin()
	{
		if (SELECTEDSINS == null)
		{
			SELECTEDSINS = new List<string>();
		}
		if (SINS == null)
		{
			SINS = new List<string>();
			SINS.Add("Ira");
			SINS.Add("Orgulho");
			SINS.Add("Luxuria");
			SINS.Add("Inveja");
			SINS.Add("Preguica");
			SINS.Add("Avareza");
			SINS.Add("Gula");
		}
		
		//randomSin();
		
		//Debug.Log (Sin);
		
		//Debug.Log("______________________");
	}
	
	void randomSin()
	{
		while(sinString == "")
		{
			int index; 
			string tempSin;
			index = Random.Range(0,7);
			tempSin = SINS[index];	
			if ( SELECTEDSINS.IndexOf(tempSin) < 0)
			{
				sinString = tempSin;
			}
			
		}
		
		// Key info
		SELECTEDSINS.Add(sinString);
		if ( sinString == "Inveja")
		{
			Virtue1 = "Diligencia" ;
			Virtue2 = "Caridade";
		}
		if ( sinString == "Orgulho")
		{
			Virtue1 = "Humildade" ;
			Virtue2 = "Castidade";
		}
		if ( sinString == "Ira")
		{
			Virtue1 = "Paciencia" ;
			Virtue2 = "Humildade";
		}
		if ( sinString == "Luxuria")
		{
			Virtue1 = "Castidade" ;
			Virtue2 = "Caridade";
		}
		if ( sinString == "Gula")
		{
			Virtue1 = "Temperanca" ;
			Virtue2 = "Paciencia";
		}
		if ( sinString == "Preguica")
		{
			Virtue1 = "Magnamidade" ;
			Virtue2 = "Diligencia";
		}
		if ( sinString == "Avareza")
		{
			Virtue1 = "Magnamidade" ;
			Virtue2 = "Temperanca";
		}
	}

	public void setSin(HistoryHolder.Sins enumSin)
	{
	
		
		Debug.Log (enumSin);
		if ( enumSin == HistoryHolder.Sins.Inveja)
		{
			Sin 	= enumSin;
			sinString = "Inveja";
			Virtue1 = "Diligencia" ;
			Virtue2 = "Caridade";
		}
		if ( enumSin == HistoryHolder.Sins.Oruglho)
		{
			Sin 	= enumSin;
			sinString = "Orgulho";
			Virtue1 = "Humildade" ;
			Virtue2 = "Castidade";
		}
		if ( enumSin == HistoryHolder.Sins.Ira )
		{
			Sin		= enumSin;
			sinString = "Ira";
			Virtue1 = "Paciencia" ;
			Virtue2 = "Humildade";
			
		}
		if ( enumSin == HistoryHolder.Sins.Luxuria)
		{
			Sin		= enumSin;
			sinString = "Luxuria";
			Virtue1 = "Castidade" ;
			Virtue2 = "Caridade";
		}
		if ( enumSin == HistoryHolder.Sins.Gula)
		{
			Sin		= enumSin;
			sinString = "Gula";
			Virtue1 = "Temperanca" ;
			Virtue2 = "Paciencia";
		}
		if ( enumSin == HistoryHolder.Sins.Preguica)
		{
			Sin		= enumSin;
			sinString = "Preguica";
			Virtue1 = "Magnamidade" ;
			Virtue2 = "Diligencia";
		}
		if ( enumSin == HistoryHolder.Sins.Avareza)
		{
			Sin 	= enumSin;
			sinString = "Avareza";
			Virtue1 = "Magnamidade" ;
			Virtue2 = "Temperanca"; 
		}
	}

	
	public static void VERIFYWEAKNESS()
	{
	
	}
	
	
	//pega o pecado do level e verifica com a moral do player
	public static bool VERIFYSTRONG(string levelSin, string virtue1, string virtue2 )
	{
	
		Debug.Log (levelSin);
		Debug.Log (virtue1);
		Debug.Log (virtue2);
		bool returnBool = false;
		if ( (levelSin == "Ira" && virtue1 == "Diligencia") ||(levelSin == "Ira" && virtue2 == "Diligencia")  )
		{
			returnBool = true;
		}
		if ((levelSin == "Orgulho" && virtue1 == "Magnamidade") ||(levelSin == "Orgulho" && virtue2 == "Magnamidade"))
		{
			returnBool = true;
		}
		if ( (levelSin == "Luxuria" && virtue1 == "Temperanca") ||(levelSin == "Luxuria" && virtue2 == "Temperanca"))
		{
		Debug.Log ("aa");
			returnBool = true;	
		}
		if ( (levelSin == "Inveja" && virtue1 == "Paciencia") ||(levelSin == "Inveja" && virtue2 == "Paciencia"))
		{
			returnBool = true;
		}
		if ( (levelSin == "Preguica" && virtue1 == "Humildade") ||(levelSin == "Preguica" && virtue2 == "Humildade"))
		{	
			returnBool = true;
		}
		if ((levelSin == "Avareza" && virtue1 == "Castidade") ||(levelSin == "Avareza" && virtue2 == "Castidade"))
		{
			returnBool = true;
		}
		if ( (levelSin == "Gula" && virtue1 == "Caridade") ||(levelSin == "Gula" && virtue2 == "Caridade"))
		{
			returnBool = true;
		}
		
		return returnBool;
		
	}
	
	// comentarios indicam o oposto
	public void setTouchedSin(string Sin)
	{
		if ( Sin == "Inveja") //Paciencia
		{
			touchedSin = HistoryHolder.Sins.Inveja;
		}
		if ( Sin == "Orgulho") //Magnamidade
		{
			touchedSin = HistoryHolder.Sins.Oruglho;
		}
		if ( Sin == "Ira") //Diligencia
		{
			touchedSin = HistoryHolder.Sins.Ira;
			
		}
		if ( Sin == "Luxuria") //Temperança
		{
			touchedSin = HistoryHolder.Sins.Luxuria;
		}
		if ( Sin == "Gula") //Caridade
		{
			
			touchedSin = HistoryHolder.Sins.Gula;
		}
		if ( Sin == "Preguica") //Humildade
		{
			touchedSin = HistoryHolder.Sins.Preguica;
		}
		if ( Sin == "Avareza") //Castidade
		{
			touchedSin = HistoryHolder.Sins.Avareza;
		}
	}
	
	
	
}
