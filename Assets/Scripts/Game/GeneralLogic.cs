﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GeneralLogic : MonoBehaviour {


	public HistoryHolder storyHolder;

	public Stats stats;
	public DialogStructure personagem1;
	public DialogStructure personagem2;
	public DialogStructure personagem3;
	public DialogStructure personagem4;

	public string currentDay = "Day";
	public int Day = 1;
	public bool playIntro = false;
	
	public static bool ALLINTERATIONSLOCKED = false;
	
	public GameObject selectedPlayer;
	public static GeneralLogic instance;

	public Image rect;	
	public List<Character> characters;	
	public GameObject characterGroup;
	
	public GameObject Ascensorista;
	
	public Text coinText;
	public int quantCoins = 8;
	
	public bool exploredFloor = false;
	
	public int heavenBesideYou = 0;
	public bool fullhell = false;
	
	
	public bool SKIPINTRO;
	
	void Start () {
		
		stats.rightKiller = false;
		
		//storyHolder
		
		
		instance = this;
		
		
		Ascensorista = 
			GameObject.FindGameObjectWithTag("Ascensorista");
		
		
		
		
		StartCoroutine("gambiarra");
		
	}
	
	
	IEnumerator gambiarra()
	{
		yield return new WaitForSeconds(0.1f);
		quantCoins = Variables.instance.QUANTCOINS;
		GeneralLogic.instance.coinText.text = "" + GeneralLogic.instance.quantCoins;
		AddToElevatorCharacters.instance.startList = storyHolder.EntranceDialog;
		
		StopCoroutine("gambiarra");
		characterGroup.GetComponent<Animator>().Play("animGroup");
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
		
	public void decreasePlayers()
	{
		
	}
	
	
	IEnumerator restartRoutine()
	{
		yield return new WaitForSeconds(5);
			
		Elevator.instance.transform.position = Vector3.zero;
	
	}
	
	public static void LOCKALLINTERATIONS()
	{
		ALLINTERATIONSLOCKED = true;
	}
	
	public static void UNLOCKALLINTERATIONS()
	{
		ALLINTERATIONSLOCKED = false;
	}
	
			
	
}
