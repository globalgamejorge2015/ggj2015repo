﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {



	public static CameraScript instance;
	public GameObject elevator;
	Transform cachedTransform;
	Transform cachedElevatorPosition;
	public bool focused;
	Vector3 unfocusedPosition = new Vector3(0.04f, 16.4f, -28.5f);
	public new Camera camera;	
	

	// Use this for initialization
	void Start () {
		camera = GetComponent<Camera>();
		cachedTransform = transform;
		cachedElevatorPosition = elevator.transform;
		instance = this;
		
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
		
		
			if ( focused)
			{
				cachedTransform.position -= (cachedTransform.position - (cachedElevatorPosition.position + new Vector3(-0.08f,1.5f,-4.57f))) * 0.1f;
				camera.orthographicSize -= (camera.orthographicSize -5) * 0.1f;
			}
			else
			{
				cachedTransform.position -= (cachedTransform.position - ( unfocusedPosition)) * 0.1f;
				camera.orthographicSize -= (camera.orthographicSize -20) * 0.1f;
		}
		
	}
	
	public void changeFocus()
	{
		if ( !GeneralLogic.ALLINTERATIONSLOCKED)
		{
			//if ( Elevator.MAYCHANGEFLOOR)
				focused = !focused;
				
			
		}
	}
	
	
}
