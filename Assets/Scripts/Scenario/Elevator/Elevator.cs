﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Elevator : MonoBehaviour {

	public GameObject pointToStay;
	public Vector3 pointToStayPosition;
	
	Transform cachedTransform;
//	Vector3 positionHeaded = Vector3.zero;
	public static bool MAYCHANGEFLOOR = true;
	bool changeFloor = false;
	public List<Transform> players;
	public static Elevator instance;
	public static string currentFloor;
	public static int PLAYERSINELEVATOR = 4;
	public Transform skyPoint;
	public Transform hellPoint;
	string floor;
	
	
	// Use this for initialization
	void Start () {
		instance = this;
		cachedTransform = transform;
		//cachedTransform.position = pointToStay.transform.position;
		
	}
	
	// Update is called once per frame
	void Update () {
	
		if (!GeneralLogic.ALLINTERATIONSLOCKED)
		{
			if ( changeFloor)
			{
				changingFloor(pointToStayPosition);
			}
		}
	}
	
	public void changeElevatorFloor(Transform passPosition)
	{
		Debug.Log("asdijuajsdiua");
		StartCoroutine("executeElevator", passPosition);
		Sharon.instance.playChangeFloorAnim();
		
	}
	
	public IEnumerator executeElevator(Transform passPosition)
	{
	
		while (!Sharon.instance.handledHandler)
		{
			yield return new WaitForSeconds(0.1f);
			
		}
		
		
		
		Sharon.instance.disableHandler();
		if (floor != passPosition.gameObject.name && !changeFloor)
		{
			if (!GeneralLogic.ALLINTERATIONSLOCKED && (GeneralLogic.instance.quantCoins > 0 || (passPosition.gameObject.name == "Inferno" || passPosition.gameObject.name == "Ceu" )))
			{
				floor = passPosition.gameObject.name;
				currentFloor = passPosition.name;
				
				if (passPosition.gameObject.name == "Inferno" || passPosition.gameObject.name == "Ceu")
				{
					//changeFloor = true;
					currentFloor = passPosition.name;
				}
				else
				{
					GeneralLogic.instance.quantCoins--;
					GeneralLogic.instance.exploredFloor = false;
				}
				GeneralLogic.instance.coinText.text = "" + GeneralLogic.instance.quantCoins;
				if ( MAYCHANGEFLOOR)
				{
					pointToStayPosition = passPosition.position;
					MAYCHANGEFLOOR = false;
					
					
					
					changeFloor = true;
				}
			}
		}
	
		StopCoroutine("executeElevator");
	
	}
	
	public void skyFloor()
	{
		StartCoroutine("executeElevator", skyPoint);
		Sharon.instance.playChangeFloorAnim();
	}
	
	public void hellFloor()
	{
		StartCoroutine("executeElevator", hellPoint);
		Sharon.instance.playChangeFloorAnim();
	}
	
	void changingFloor(Vector3 goTo)
	{
		
		Vector3 speed = (cachedTransform.position - goTo) * 0.07f;
		
		if ( Vector3.Distance(speed,Vector3.zero) > 0.1f)
		{
			int sentido;
			if ( speed.y < 0)
				sentido = -1;
			else
				sentido = 1;
			
			
			speed = new Vector3(0, 0.07f, 0 ) * sentido;
			
		}
		
		cachedTransform.position -= speed;
		
		//Debug.Log ( (cachedTransform.position - goTo) * 0.1f);
		
		foreach (Transform t in players)
		{
			t.position -= speed;
		}
		
		if ( Vector3.Distance(cachedTransform.position, goTo) < 0.1f)
		{
			changeFloor = false;
			MAYCHANGEFLOOR = true;
		}
		
		
	}
	
}
