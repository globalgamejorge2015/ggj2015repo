﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using System.Collections;
#if UNITY_EDITOR

public class Wizard_SetHistory : ScriptableWizard {


	GameObject gLogic; 
	
	public HistoryHolder gLogicHistory;
	

	// Use this for initialization
	[MenuItem ("Story Wizard/History Setter Wizard")]
	static void CreateWizard () {
	
	
	
		ScriptableWizard.DisplayWizard<Wizard_SetHistory>("Create Dialog", "Close");
	
	}
	
	void OnEnable()
	{
		
		gLogic = GameObject.FindGameObjectWithTag("GeneralLogic");
		
		gLogicHistory =  gLogic.GetComponent<GeneralLogic>().storyHolder;
	}
	
	
	void OnWizardUpdate () {
		
		helpString = "Create your story!";
		
		gLogic.GetComponent<GeneralLogic>().storyHolder = gLogicHistory;
		
		
	}   
	
	
}
#endif
