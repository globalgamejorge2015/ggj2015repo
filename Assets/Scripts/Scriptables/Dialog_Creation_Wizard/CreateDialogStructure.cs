﻿// C#
// Creates a simple wizard that lets you create a Light GameObject
// or if the user clicks in "Apply", it will set the color of the currently
// object selected to red
#if UNITY_EDITOR


using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

public class WizardCreateDialog : ScriptableWizard {

	
	
	
	public new string title ;
	
	
	
	
	
	[MenuItem ("Story Wizard/Create Story Wizard")]
	static void CreateWizard () {
		
		
	
		
		
		ScriptableWizard.DisplayWizard<WizardCreateDialog>("Create Dialog", "Close", "Apply");
		//If you don't want to use the secondary button simply leave it out:
		//ScriptableWizard.DisplayWizard<WizardCreateLight>("Create Light", "Create");
	}	
	
	void OnEnable()
	{
		
	}
	
	void OnWizardCreate () {
	
	
		
		
		
				
	}
	
	void OnWizardUpdate () {
		
		helpString = "Create your story!";
		
		
		
	}   
	// When the user pressed the "Apply" button OnWizardOtherButton is called.
	
	void OnWizardOtherButton()
	{
	
			// do something
			// inserir protecao de sobrescrita
			string assetPath = "Assets/Histories/Resources/"+ title +".asset";
			
			HistoryHolder scriptableObject = ScriptableObject.CreateInstance<HistoryHolder>();
			
			AssetDatabase.CreateAsset(scriptableObject, assetPath);
			
			AssetDatabase.SaveAssets();
			
			
			EditorUtility.FocusProjectWindow();
			
			Selection.activeObject = scriptableObject;
			
		
	
	
	}
	
}
#endif
