﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class SharonDialogs {
	
	public string[] neutralResponse;
	public string[] goodResponse;
	public string[] badResponse;

	
	public string[] sendToHeaven;
	public string[] sendToHell;
	public string[] FullHell;
	public string[] vaiProInfernoPqTeAchei;
	public string[] onePerFloor;
	public string[] heavenConsideration;
 	
	
	
	
	public string[] exitLine;

}
