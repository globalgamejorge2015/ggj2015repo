﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class GameDialog {

	
	public enum Characters{Cavaleiro,Bobo,Idiota,Princesa};
	public Characters Personagem;
	 
	public List<IndividualDialog> Assassino_RespostaNeutra;
	public List<IndividualDialog> Assassino_RespostaPositiva;
	public List<IndividualDialog> Assassino_RespostaIrritada;
	
	public List<IndividualDialog> Joker_RespostaNeutra;
	public List<IndividualDialog> Joker_RespostaPositiva;
	public List<IndividualDialog> Joker_RespostaIrritada;
	
	public List<IndividualDialog> Neutro_RespostaNeutra;
	public List<IndividualDialog> Neutro_RespostaPositiva;
	public List<IndividualDialog> Neutro_RespostaIrritada;
	
	
}
