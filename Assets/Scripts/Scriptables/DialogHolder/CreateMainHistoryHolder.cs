﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class CreateMainHistoryHolder : ScriptableObject 
{
	#if UNITY_EDITOR
	private const string assetPath = "Assets/Histories/Resources/Histories_assets.asset";
	
	//[MenuItem("Story Wizard/Create History Holder")]
	public static void CreateHUDSettings()
	{
		
		HistoryHolder scriptableObject = ScriptableObject.CreateInstance<HistoryHolder>();
		
		AssetDatabase.CreateAsset(scriptableObject, assetPath);
		
		AssetDatabase.SaveAssets();
		
		
		EditorUtility.FocusProjectWindow();
		
		Selection.activeObject = scriptableObject;
	}
	#endif
	
	
}
