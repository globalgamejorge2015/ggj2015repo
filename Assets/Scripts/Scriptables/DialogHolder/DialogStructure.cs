﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DialogStructure : ScriptableObject {
	
	
	
	public List<IndividualDialog> neutralDialogsNeutralSuspect;
	public List<IndividualDialog> neutralDialogsNeutralJoker;
	public List<IndividualDialog> neutralDialogsNeutralMurder;
	
	public List<IndividualDialog> angryDialogsSuspect;
	public List<IndividualDialog> angryDialogsJoker;
	public List<IndividualDialog> angryDialogsMurder;
	
	public List<IndividualDialog> goodDialogsSuspect;
	public List<IndividualDialog> goodDialogsJoker;
	public List<IndividualDialog> goodDialogsMurder;
	
	
	
	
	
}
