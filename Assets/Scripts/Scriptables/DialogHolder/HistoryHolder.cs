﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class HistoryHolder : ScriptableObject {
	
	public enum Sins{Luxuria,Inveja,Ira,Avareza,Oruglho,Gula,Preguica,RandomSin};
	
	public enum Personagem{Assassino,Joker,Neutro,Randomico};
	
	
	public Personagem cavaleiro;
	public Sins PecadoCavaleiro;
	
	public Personagem idiota;	
	public Sins PecadoIdiota;
	
	public Personagem princesa;
	public Sins PecadoPrincesa;	
	
	public Personagem bobo  = Personagem.Joker;
	public Sins PecadoBobo;	
	
	
	public List<EntranceDialog> EntranceDialog;
	public List<GameDialog> GameDialogs;
	
	public List<SharonDialogs> sharonDialogs;
	
	
	
	
	
}
