﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Speak : MonoBehaviour {

	// Use this for initialization
	
	public Image img;
	public Image clickImage;
	public Text textBox;
	bool validateNextString = false;
	string coroutineName;
	//Character charac;
	Sharon sharonInstance;
	
	string[] speech;
	
	public void CustomStart () {
		//charac = GetComponent<Character>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void makeSpeech(string[] speech, Sharon instance, Character who, string coroutineName)
	{
		
		Debug.Log ("teste");
		
		StopCoroutine("DisapearText");
		if ( !img.enabled)
		{
			img.enabled 	= true;
			textBox.enabled = true;
			
			
		}
		img.color 			= new Color(img.color.r,img.color.g,img.color.b,255);
		textBox.color 		= new Color(textBox.color.r,textBox.color.g,textBox.color.b,255);
		clickImage.color	= new Color(clickImage.color.r,clickImage.color.g,clickImage.color.b,255);
		textBox.text		= speech[0];
		
		this.speech 		= speech;
		
		if ( sharonInstance == null)
			sharonInstance = instance;
			
		this.coroutineName = coroutineName;
				
				
		StartCoroutine("nextDialog");
	}
	
	public IEnumerator nextDialog()
	{
		yield return new WaitForSeconds(0.1f);
		int i = 1;
		bool stopCoroutine = false;
		while ( !stopCoroutine)
		{
			while(	!Input.GetMouseButtonDown(0))
			{
			
				
				yield return new WaitForSeconds(0.01f);
			}
			Debug.Log ("passou");
			Debug.Log( speech.Length);
			
			validateNextString = true;
			
			
			if ( speech.Length == i)
			{
				stopCoroutine = true;
			}
			else
			{
				textBox.text = speech[i];
				yield return new WaitForSeconds(0.1f);
				i++;
				
				
			}
			
			
		}
		
		if ( gameObject.GetComponent<Sinners>() != null)
			gameObject.GetComponent<Sinners>().dealingWithStuff(coroutineName);
		
		
		
		StartCoroutine("DisapearText");
	}
	
	public void speeakDialog(string speech, string kindOfSpeech, Character who)
	{
		
	//	StartCoroutine("DisapearText");
		
		StopCoroutine("nextDialog");
		//StopCoroutine("DisapearText");
		if ( !img.enabled)
		{
			img.enabled 	= true;
			textBox.enabled = true;	
		}
		
		img.color 			= new Color(img.color.r,img.color.g,img.color.b,255);
		textBox.color 		= new Color(textBox.color.r,textBox.color.g,textBox.color.b,255);
		clickImage.color	= new Color(clickImage.color.r,clickImage.color.g,clickImage.color.b,255);
		textBox.text		= speech;
		
	}
	
	
	
	
	IEnumerator DisapearText()
	{
		StopCoroutine("nextDialog");
		
		img.color 			= new Color(img.color.r,img.color.g,img.color.b,1);
		textBox.color 		= new Color(textBox.color.r,textBox.color.g,textBox.color.b,1);
		clickImage.color	= new Color(clickImage.color.r,clickImage.color.g,clickImage.color.b,1);
		while (img.color.a > 0)
		{
			img.color 		-= new Color(0,0,0,0.16f);
			textBox.color 	-= new Color(0,0,0,0.16f);
			clickImage.color-=  new Color(0,0,0,0.16f);
			yield return new WaitForSeconds(0.03f);
			
		}
		
		img.color 			= new Color(img.color.r,img.color.g,img.color.b,0);
		textBox.color 		= new Color(textBox.color.r,textBox.color.g,textBox.color.b,0);
		clickImage.color	= new Color(clickImage.color.r,clickImage.color.g,clickImage.color.b,0);
		
		yield return new WaitForSeconds(2.3f);
		StopCoroutine("DisapearText");
			
					
	}
	
}
