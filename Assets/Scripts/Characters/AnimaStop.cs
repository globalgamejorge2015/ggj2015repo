﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AnimaStop : MonoBehaviour {


	public Animator anim;
	public Text text;
	Image img;
	public Vector3 orinPos;
	
	// Use this for initialization
	void Start () {
		
		orinPos = transform.position;
		anim = GetComponent<Animator>();
		img = GetComponent<Image>();
		//text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	
		if (text != null)
		{
			text.color = 
			new Vector4(
			text.color.r,
			text.color.g,
			text.color.b,
			 img.color.a);
		}	 
	}
	
	
	public void stop()
	{
		anim.enabled = false;
		
	}
	
	public void getOut()
	{
		
		transform.position  += new Vector3(500,4000,0);
		
	}
	
	public void GetIn()
	{
		transform.position = orinPos + new Vector3(458,350);
	
	}
	
}
