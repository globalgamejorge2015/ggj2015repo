﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AddToElevatorCharacters : MonoBehaviour {


	public List<EntranceDialog> startList;
	public GameObject Ascensorista;
	Vector3 orinPos;
	public static AddToElevatorCharacters instance;
	int readIndex = 0;
	bool changedFocus = false;
	
	
	// Use this for initialization
	void Start () {
	
		instance = this;
		orinPos = transform.position;
	
	}
	

	
	public void addToElevator()
	{
		foreach (Transform t in this.transform ) 
		{
		
			Elevator.instance.players.Add(t);
		
		}
		
		
		for (int i = 1 ; i < 5 ; i++)
		{
			Elevator.instance.players[i].GetComponent<Sinners>().changeAnim("Idle");
		}
		
		if ( Variables.instance.SKIPINTRO)
		{
			GeneralLogic.UNLOCKALLINTERATIONS();
		}
		else
			StartCoroutine("firstSpeak");
		
	}
	
	
	Speak tempSpeak; // declarar dentro do escopo nao funciona O.o
	IEnumerator firstSpeak()
	{
	
		if (!changedFocus)
		{
			CameraScript.instance.changeFocus();
			GeneralLogic.LOCKALLINTERATIONS();
		}
			
			
		yield return new WaitForSeconds(0.1f);
		
		if ( startList[readIndex].Personagem == EntranceDialog.CharacterType.Sharon)
		{
			Ascensorista.GetComponent<Speak>().speeakDialog(startList[readIndex].linha,"",(Ascensorista.GetComponent<Character>() ));
			tempSpeak = Ascensorista.GetComponent<Speak>();
		}
		
		if ( startList[readIndex].Personagem == EntranceDialog.CharacterType.Cavaleiro)
		{
			Elevator.instance.players[1].GetComponent<Speak>().speeakDialog(startList[readIndex].linha,"",(Elevator.instance.players[1].GetComponent<Character>() ));
			tempSpeak = Elevator.instance.players[1].GetComponent<Speak>();
		}
		
		if ( startList[readIndex].Personagem == EntranceDialog.CharacterType.Bobo)
		{
			Elevator.instance.players[2].GetComponent<Speak>().speeakDialog(startList[readIndex].linha,"",(Elevator.instance.players[2].GetComponent<Character>() ));
			tempSpeak = Elevator.instance.players[2].GetComponent<Speak>();
		}
		
		if ( startList[readIndex].Personagem == EntranceDialog.CharacterType.Princesa)
		{
			Elevator.instance.players[3].GetComponent<Speak>().speeakDialog(startList[readIndex].linha,"",(Elevator.instance.players[3].GetComponent<Character>() ));
			tempSpeak = Elevator.instance.players[3].GetComponent<Speak>();
		}
		
		if ( startList[readIndex].Personagem == EntranceDialog.CharacterType.Idiota)
		{
			Elevator.instance.players[4].GetComponent<Speak>().speeakDialog(startList[readIndex].linha,"",(Elevator.instance.players[4].GetComponent<Character>() ));
			tempSpeak = Elevator.instance.players[4].GetComponent<Speak>();
		}
		
		
		while(	!Input.GetMouseButtonDown(0))
		{
			
			yield return new WaitForSeconds(0.01f);
		}
		
		if ( tempSpeak != null)
			tempSpeak.StartCoroutine("DisapearText");
		StopCoroutine("firstSpeak");
		//StartCoroutine("secondSpeak");
		
		//startList.RemoveAt(0);
		readIndex++;
		if ( readIndex < startList.Count)
		{
		
			StartCoroutine("firstSpeak");
		}
		else
		{
			GeneralLogic.UNLOCKALLINTERATIONS();
		}
		
		//GeneralLogic.UNLOCKALLINTERATIONS();
		
		
	}
	
	public void returnOrinPos()
	{
		
		transform.position = orinPos;
		
	}
	
	
}
