﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Sinners : Character {
	
	
	
	
	
	
	public DialogStructure dialogs;
	//public new List<string> responses;
	
	
	// 0 ruim // 1 joker //
	//  2 neutro 3 neutro	//
	public new static List<bool> Morals;
	
	int way = 1;
	bool onCoRoutine = false;
	bool backToPlace = false;
	public string consequence;
	public PowerSin sinsVirtues = new PowerSin();
	
	
	Animator anim;
	
	
	
	
	
	public void Start () {
		
		if ( anim == null)
			anim = gameObject.GetComponent<Animator>();
		
		speak = GetComponent<Speak>();
		if ( Morals == null)
		{
			Morals = new List<bool>();
			Morals.Add (false);
			Morals.Add (false);
			Morals.Add (false);
			Morals.Add (false);
		}
		
			mHandler = new MaterialHandler(gameObject,materials);
			
			mHandler.changeColor(0);
			
			//sinsVirtues = new PowerSin();
			cachedTransform = transform;
			GeneralLogic.instance.characters.Add(this);
			orinPos = cachedTransform.position;
			moral = AbsoluteMoral.Neutral;
			//Debug.Log (moral);
			//Debug.Log (sinsVirtues.Sin);

			changeAnim("Walking");
						
	}
	
	
	public void changeAnim(string str)
	{
		// string contendo animacao, junto ao ID que DEVE ser o nome da personagem
		// primeira letra da animacao SEMPRE maiuscula
		if (anim != null)
		{
			anim = gameObject.GetComponent<Animator>();
			//anim.Get
			//Debug.Log (anim.Ha("princessWalking"));
		//	try
			{
				//Debug.Log(name + str);
				anim.Play(name + str);
				//anim.Play("mortisWalking",0);
				
				
				
				//Debug.Log (anim);
			}
		//	catch (UnityException e)
			{
				
			}
		}
	}
	
	public void SetMoral(HistoryHolder.Personagem value )
	{
		//Debug.Log (value);
		if (value == HistoryHolder.Personagem.Assassino)
		{
			//Debug.Log ("1");
			//Debug.Log (name);
			moral = AbsoluteMoral.Bad;
		}
		else if (value == HistoryHolder.Personagem.Joker)
		{
			//Debug.Log ("2");
			//Debug.Log (name);
			moral = AbsoluteMoral.Joker;
		}
		else if ( value == HistoryHolder.Personagem.Neutro)
		{
			//Debug.Log ("3");
			//Debug.Log (name);	
			moral = AbsoluteMoral.Neutral;
		}
		else if (value == HistoryHolder.Personagem.Randomico)
		{
			int i = Random.Range(0,4);
			
			if (i == 0)
				moral = AbsoluteMoral.Bad;
			if (i == 1)
				moral = AbsoluteMoral.Joker;
			if (i == 2)
				moral = AbsoluteMoral.Neutral;
			if (i == 3)
				moral = AbsoluteMoral.Neutral;
			
		}
		
		
	}
	
	
	
	// Update is called once per frame
	void Update () {
		
		if (  vectDest != Vector3.zero )
		{	
		
			Debug.Log(Vector3.Distance(cachedTransform.position, vectDest));
			
			if ( Vector3.Distance(cachedTransform.position, vectDest) > 0.1f)
			{
				
				cachedTransform.position -= new Vector3(Variables.instance.CHARACVELOCITY,0,0) * way * Time.deltaTime;
				
			}
			else if (Elevator.currentFloor == "Ceu")
			{
				//GeneralLogic.instance.quantCoins++;
				
				//GeneralLogic.instance.Ascensorista.GetComponent<Speak>().makeSpeech("Another resting soul... Let's move on","","");
			//	sharonInstance.StartCoroutine("SendToHeaven");
				//sharonInstance.sendToHeaven();
				
				if (GeneralLogic.instance.heavenBesideYou == 3)
				{
					string[] fooSpeech;
					fooSpeech = new string[1];
					fooSpeech[0] = "Bom, so ha um lugar para você...";
					sharonInstance.sharonSpeak.makeSpeech(fooSpeech, sharonInstance, null,"");
					Elevator.instance.changeElevatorFloor(GameObject.FindGameObjectWithTag("InfernoPoint").transform);
				}
				
				GeneralLogic.UNLOCKALLINTERATIONS();
				GeneralLogic.instance.selectedPlayer = null;
				ReferenceToUIRecord.instance.changeProfileSettings(null, "Juan");
				vectDest = Vector3.zero;
				verifyEndGame();
				return;
				
			}
			else if (Elevator.currentFloor == "Inferno")
			{
				
				//GeneralLogic.instance.quantCoins++;
				
				
				//sharonInstance.sharonSpeak.makeSpeech(sharonInstance.sharonDialog[0].sendToHell,sharonInstance,this,"");
				
				
				// end params
				Debug.Log (moral);
				
				if (moral == AbsoluteMoral.Bad)
				{
					GeneralLogic.instance.stats.rightKiller = true;
				}
				
				
				GeneralLogic.UNLOCKALLINTERATIONS();
				GeneralLogic.instance.selectedPlayer = null;
				ReferenceToUIRecord.instance.changeProfileSettings(null, "Juan");
				vectDest = Vector3.zero;
				verifyEndGame();
				return;
			}
			else if ( !onCoRoutine )
			{
				onCoRoutine = true;
				StartCoroutine("switchDialogue");
				//StartCoroutine("dealingWithStuff");
			}
			else if ( backToPlace)
			{
				changeAnim("Idle");
				cachedTransform.position = orinPos;
				backToPlace = false;
				onCoRoutine = false;
				GeneralLogic.UNLOCKALLINTERATIONS();
				way = -way;
				vectDest = Vector3.zero;
				mHandler.changeColor(0);
				if ( GeneralLogic.instance.quantCoins == 0 )
				{
					
					Elevator.instance.skyFloor();
					
				}
				
				
			}
		}
		
		
	} 
	
	void verifyEndGame()
	{
		
		if(GeneralLogic.instance.heavenBesideYou == 3 && GeneralLogic.instance.fullhell == true )
		{
			GeneralLogic.LOCKALLINTERATIONS();
			
			sharonInstance.StartCoroutine("endGame");
			
			
		}
		
	}
	
	
	IEnumerator switchDialogue()
	{
		int i = 0;
		int k = 0;
		float stop = 2.5f;
		
		changeAnim("Idle");
		if (consequence == "sinned")
		{
			
			if (moral == AbsoluteMoral.Joker)
			{
				
				for ( i =0; i < dialogs.angryDialogsJoker.Count; i++)
				{
					if ( dialogs.angryDialogsJoker[i].Sin == sinsVirtues.touchedSin )
						//for (k = 0; k< dialogs.angryDialogsJoker[i].linha.Length; k++)
						{
						
						speak.makeSpeech(dialogs.angryDialogsJoker[i].linha,sharonInstance,this,"sarcastic");
							//yield return new WaitForSeconds(stop);
						}
				}
			}
			else if (moral == AbsoluteMoral.Bad)
			{
				for ( i =0; i < dialogs.angryDialogsMurder.Count; i++)
					if ( dialogs.angryDialogsMurder[i].Sin == sinsVirtues.touchedSin )
				{
						//for (k = 0; k< dialogs.angryDialogsMurder[i].linha.Length;k++)
						{
							
							speak.makeSpeech(dialogs.angryDialogsMurder[i].linha,sharonInstance,this,"sarcastic");
							//yield return new WaitForSeconds(stop);
						}
					}
			}
			else if ( moral == AbsoluteMoral.Neutral)
			{
				for ( i =0; i < dialogs.angryDialogsSuspect.Count; i++)
					if ( dialogs.angryDialogsSuspect[i].Sin == sinsVirtues.touchedSin )
						{
						//for (k = 0; k< dialogs.angryDialogsSuspect[i].linha.Length;k++)
						{
						
						speak.makeSpeech(dialogs.angryDialogsSuspect[i].linha,sharonInstance,this,"sarcastic");
							//yield return new WaitForSeconds(stop);
						}
					}
			}  
			
			//sharonInstance.StartCoroutine("sarcastic");
		}
		else if ( consequence == "relaxed")
		{
			
			if (moral == AbsoluteMoral.Joker)
			{
				
				for ( i =0; i < dialogs.goodDialogsJoker.Count; i++)
					if ( dialogs.goodDialogsJoker[i].Sin == sinsVirtues.touchedSin )
				{
						//for (k = 0; k< dialogs.goodDialogsJoker[i].linha.Length;k++)
						{
						speak.makeSpeech(dialogs.goodDialogsJoker[i].linha,sharonInstance,this,"sarcastic");
						//	yield return new WaitForSeconds(stop);
						}
					}
			}
			else if (moral == AbsoluteMoral.Bad)
			{
				
				for ( i =0; i < dialogs.goodDialogsMurder.Count; i++)
					if ( dialogs.goodDialogsMurder[i].Sin == sinsVirtues.touchedSin )
					{
						//for (k = 0; k< dialogs.goodDialogsMurder[i].linha.Length;k++)
					{
						speak.makeSpeech(dialogs.goodDialogsMurder[i].linha,sharonInstance,this,"sarcastic");
						//	yield return new WaitForSeconds(stop);
						}
					}
			}
			else if ( moral == AbsoluteMoral.Neutral)
			{
				Debug.Log (sinsVirtues.touchedSin);
				for ( i =0; i < dialogs.goodDialogsSuspect.Count; i++)
					if ( dialogs.goodDialogsSuspect[i].Sin == sinsVirtues.touchedSin )
				{
						//for (k = 0; k< dialogs.goodDialogsSuspect[i].linha.Length;k++)
					{
						speak.makeSpeech(dialogs.goodDialogsSuspect[i].linha,sharonInstance,this,"sarcastic");
						//	yield return new WaitForSeconds(stop);
						}
					}
			}  
			
		//	sharonInstance.StartCoroutine("sarcastic");
		}
		else if ( consequence == "neutral")
		{	
			
			if (moral == AbsoluteMoral.Joker)
			{
				for ( i =0; i < dialogs.neutralDialogsNeutralJoker.Count; i++)
					if ( dialogs.neutralDialogsNeutralJoker[i].Sin == sinsVirtues.touchedSin )
					{
						//for (k = 0; k< dialogs.neutralDialogsNeutralJoker[i].linha.Length;k++)
						{
						speak.makeSpeech(dialogs.neutralDialogsNeutralJoker[i].linha,sharonInstance,this,"okay");
						//yield return new WaitForSeconds(stop);
						}
					}
			}
			else if (moral == AbsoluteMoral.Bad)
			{
				for ( i =0; i < dialogs.neutralDialogsNeutralMurder.Count; i++)
					if ( dialogs.neutralDialogsNeutralMurder[i].Sin == sinsVirtues.touchedSin )
				{
						//for (k = 0; k< dialogs.neutralDialogsNeutralMurder[i].linha.Length;k++)
						{
						speak.makeSpeech(dialogs.neutralDialogsNeutralMurder[i].linha,sharonInstance,this,"okay");
						//yield return new WaitForSeconds(stop);
						}
					}
			}
			else if ( moral == AbsoluteMoral.Neutral)
			{
				
				for ( i =0; i < dialogs.neutralDialogsNeutralSuspect.Count; i++)
					if ( dialogs.neutralDialogsNeutralSuspect[i].Sin == sinsVirtues.touchedSin )
				{
						//for (k = 0; k< dialogs.neutralDialogsNeutralSuspect[i].linha.Length;k++)
						{
						speak.makeSpeech(dialogs.neutralDialogsNeutralSuspect[i].linha,sharonInstance,this,"okay");
						//yield return new WaitForSeconds(stop);
						}
					}
			} 
			
			//sharonInstance.StartCoroutine("okay");
			
			
		}
		
		yield return new WaitForSeconds(0);
		
	}
	
	
	
	
	
	public void dealingWithStuff(string coroutineName)
	{
		cachedTransform.localScale = new Vector3(-cachedTransform.localScale.x,
		                                         cachedTransform.localScale.y,
		                                         cachedTransform.localScale.z);
		changeAnim("Idle");
		
		changeAnim("Walking");
		//exploredFloor = 
		//operaçoes logicas
		
		
		if ( sharonInstance.transform != this.transform)
			sharonInstance.StartCoroutine(coroutineName);
		
		
		vectDest = orinPos;
		way = -way;
		backToPlace = true;
	}
	
	
	public void VERIFYMORALS(string sin)
	{
		sinsVirtues.setTouchedSin(sin);
		
		Debug.Log (sinsVirtues.sinString);
		Debug.Log (sin);
		if ( sinsVirtues.sinString == sin)
		{
			// angry
			Debug.Log (sinsVirtues.sinString);
			Debug.Log (sin);
			
			consequence = "sinned";
		}
		else if (PowerSin.VERIFYSTRONG(sin, sinsVirtues.Virtue1, sinsVirtues.Virtue2))
		{
			// happy pra caralho
			/*Debug.Log (sinsVirtues.Sin);
			
			Debug.Log (sinsVirtues.Virtue1);
			Debug.Log (sinsVirtues.Virtue2);*/
			consequence = "relaxed";
		}
		else
		{
			// kristen stewart
			consequence = "neutral";
		}
		
		
	}
	
	
	void OnMouseDown()
	{
		if ( gameObject.name != "Ascensorista")
		{	
			for (int i = 0; i < GeneralLogic.instance.characters.Count; i++)
			{
				GeneralLogic.instance.characters[i].mHandler.changeColor(0);
				
			}
		
			ReferenceToUIRecord.instance.changeProfileSettings(sprite[ID], "Juan");
			GeneralLogic.instance.selectedPlayer = gameObject;
			mHandler.changeColor(1);
			
		}
		
		
		
		
	}
	
	public void GetOffElevator()
	{
		
		
		
		if ( Elevator.currentFloor == "Ceu")
		{
		
			if (GeneralLogic.instance.heavenBesideYou <3)
			{
				cachedTransform.localScale = new Vector3(-cachedTransform.localScale.x,
				                                         cachedTransform.localScale.y,
				                                         cachedTransform.localScale.z);
				changeAnim("Walking");
				GeneralLogic.instance.heavenBesideYou++;
				orinPos = cachedTransform.position;
				vectDest = cachedTransform.position - new Vector3(12,0,0);
				GeneralLogic.LOCKALLINTERATIONS();
				
				sharonInstance.StartCoroutine("heavenConsideration");
			}
			else
			{
				// "Well, there's only one place for you..."
				sharonInstance.StartCoroutine("vaiProInferno");
			}
			return;
		}
		
		if ( Elevator.currentFloor == "Inferno")
		{
			
			
			if (!GeneralLogic.instance.fullhell )
			{
				cachedTransform.localScale = new Vector3(-cachedTransform.localScale.x,
				                                         cachedTransform.localScale.y,
				                                         cachedTransform.localScale.z);
				changeAnim("Walking");
				GeneralLogic.instance.fullhell = true;
				orinPos = cachedTransform.position;
				vectDest = cachedTransform.position - new Vector3(12,0,0);
				GeneralLogic.LOCKALLINTERATIONS();
				
				sharonInstance.StartCoroutine("sendToHell");
			}
			else
			{
				// adicionar fala na estrutura
				sharonInstance.StartCoroutine("fullHell");
			}
			return;
			
			
		}
		
		if ( !GeneralLogic.instance.exploredFloor)
		{
			GeneralLogic.instance.exploredFloor = true;
			orinPos = cachedTransform.position;
			vectDest =  new Vector3(-5.9f,cachedTransform.position.y,cachedTransform.position.z);
			//Elevator.instance.players.RemoveAt(Elevator.instance.players.IndexOf(cachedTransform));
			//GeneralLogic.instance.decreasePlayers();
			//if ( Elevator.PLAYERSINELEVATOR == 0)
			{
				//GeneralLogic.instance.endDay();
			}
			GeneralLogic.LOCKALLINTERATIONS();
		}
		else
		{
			// adicionar resposta de exceçao
			sharonInstance.StartCoroutine("onePerFloor");
		}
		
		cachedTransform.localScale = new Vector3(-cachedTransform.localScale.x,
		                                         cachedTransform.localScale.y,
		                                         cachedTransform.localScale.z);
		changeAnim("Walking");
		
	}
	
	public void GetBackToStart()
	{
		changeAnim("Walking");
		cachedTransform.position = orinPos;	
		vectDest = Vector3.zero;
	}
	
	
}
