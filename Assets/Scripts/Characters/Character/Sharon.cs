﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Sharon : Character {
	
	
	
	
	
	
	public PowerSin sinsVirtues = new PowerSin();
	public Animator anim;
	
	public Speak sharonSpeak;
	public List<SharonDialogs> sharonDialog;
	
	public static Sharon instance;
	public bool handledHandler = false;
	
	
//	public delegate offElevator;
	
	
	void Start () {
		
		
			instance = this;
			if ( anim == null)
				anim = gameObject.GetComponent<Animator>();
			sharonInstance = this;
			sharonDialog = GeneralLogic.instance.storyHolder.sharonDialogs;
			sharonSpeak = GeneralLogic.instance.Ascensorista.GetComponent<Speak>();
		
//		base.Start();
		
		anim.Play("sharonIdle");
	}
	
	
	void verifyEndGame()
	{
		
		if(GeneralLogic.instance.heavenBesideYou == 3 && GeneralLogic.instance.fullhell == true )
		{
			GeneralLogic.LOCKALLINTERATIONS();
			
			StartCoroutine("endGame");
			
			
		}
		
	}
	
	public void playChangeFloorAnim()
	{
		anim.Play("sharonChangeFloor");
	}
	
	public void enableHandler()
	{
		anim.Play("sharonIdle");
		handledHandler = true;
	}
	
	public void disableHandler()
	{
		handledHandler = false;
	}
	
	
	public IEnumerator endGame()
	{	
		//for ( int i = 0; i < sharonDialog[0].exitLine.Count; i++)
		{
		
			sharonSpeak.makeSpeech(sharonDialog[0].exitLine,this,this,"");	
			yield return new WaitForSeconds(2.5f);
			
		}
		
		Application.LoadLevel("statsScene");
		StopCoroutine("endGame");
	}
	
	IEnumerator sarcastic()
	{
		//for ( int i = 0 ; i < sharonDialog[0].goodResponse.Count; i++)
		{			
			sharonSpeak.makeSpeech(sharonDialog[0].goodResponse,this,this,"");
			
			yield return new WaitForSeconds(2.5f);
		}
		
		
		StopCoroutine("sarcastic");
	}
	
	IEnumerator okay()
	{
		
		//for ( int i = 0 ; i < sharonDialog[0].neutralResponse.Count; i++)
		{	
			sharonSpeak.makeSpeech(sharonDialog[0].neutralResponse,this,this,"");
			
			yield return new WaitForSeconds( 2.5f);
		}
		
		StopCoroutine("okay");
		
	}
	
	IEnumerator sendToHell()
	{
		//for ( int i = 0 ; i < sharonDialog[0].sendToHell.Count; i++)
		{	
			sharonSpeak.makeSpeech(sharonDialog[0].sendToHell,this,this,"");
			
			yield return new WaitForSeconds(2.5f);
		}
		
		StopCoroutine("sendToHell");
		
	}
	
	public void sendToHeaven()
	{
		//for ( int i = 0 ; i < sharonDialog[0].sendToHeaven.Count; i++)
		{	
			sharonSpeak.makeSpeech(sharonDialog[0].sendToHeaven,this,this,"");
			//yield return new WaitForSeconds(2.5f);
			
		}
		
		//StopCoroutine("sendToHeaven");
		
	}
	
	IEnumerator fullHell()
	{
		//for ( int i = 0 ; i < sharonDialog[0].FullHell.Count; i++)
		{	
			sharonSpeak.makeSpeech(sharonDialog[0].FullHell,this,this,"");
			
			yield return new WaitForSeconds( 2.5f);
		}
		
		StopCoroutine("fullHell");
		
	}
	
	IEnumerator vaiProInferno()
	{
		//for ( int i = 0 ; i < sharonDialog[0].vaiProInfernoPqTeAchei.Count; i++)
		{	
			
			
			sharonSpeak.makeSpeech(sharonDialog[0].vaiProInfernoPqTeAchei,this,this,"");
			
			yield return new WaitForSeconds( 2.5f);
		}
		
		StopCoroutine("vaiProInferno");
		
	}
	
	IEnumerator onePerFloor()
	{
		//for ( int i = 0 ; i < sharonDialog[0].onePerFloor.Count; i++)
		{	
			
			
			sharonSpeak.makeSpeech(sharonDialog[0].onePerFloor,this,this,"");
			
			yield return new WaitForSeconds( 2.5f);
		}
		
		StopCoroutine("onePerFloor");
		
	}
	
	IEnumerator heavenConsideration()
	{
		//for ( int i = 0 ; i < sharonDialog[0].heavenConsideration.Count; i++)
		{	
			
			
			sharonSpeak.makeSpeech(sharonDialog[0].heavenConsideration,this,this,"");
			
			yield return new WaitForSeconds( 2.5f);
		}
		
		StopCoroutine("heavenConsideration");
		
	}
	
	
	IEnumerator dealingWithStuff()
	{
		
		yield return new WaitForSeconds(2);
		
		
		//exploredFloor = 
		//operaçoes logicas
		
		vectDest = orinPos;
//		backToPlace = true;
		StopCoroutine ("dealingWithStuff");
	}
	
	
	
	
	
	
	
}
