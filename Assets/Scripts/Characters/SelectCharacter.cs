﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SelectCharacter : MonoBehaviour {

	//enum Type {Man,Woman};
	public GameObject textBox;
	Speak speak;
	public int Id;
	public new string name;
	Sinners charac;
	Sharon sharon;
	
	public Material[] materials;
	
	public List<Sprite> spriteList; 
	
	// Use this for initialization
	void Start () {
		
		
		
		if (Id == -1)
		{
		
			sharon =  gameObject.AddComponent<Sharon>() ;
			sharon.ID = Id;
			sharon.sprite = spriteList;
			sharon.name = name;
		}
		else
		{
			charac = gameObject.AddComponent<Sinners>();
			charac.ID = Id;
			charac.sprite = spriteList;
			charac.name = name;	
		}
		
		
		
		textBox = (GameObject)Instantiate(textBox,Vector3.zero, Quaternion.identity) as GameObject;
		textBox.GetComponent<UISettings>().referenceObject = gameObject;
		
		speak = gameObject.GetComponent<Speak>();
		speak.CustomStart();
		foreach (Transform tr in textBox.transform)
		{
			Debug.Log (tr.name);
			if (tr.name == "Text")
			{
				speak.textBox = tr.GetComponent<Text>();
				speak.img = textBox.GetComponent<Image>();
			}
			else if (tr.name == "Image")
			{
				
				speak.clickImage = tr.GetComponent<Image>();
				Debug.Log (speak.clickImage);
			}
		}
				
				
		StartCoroutine("Entry");
		
				
	}
	
	
	
	IEnumerator Entry()
	{
		yield return new WaitForSeconds(0.1f);
		
		
		if (gameObject.name != "Ascensorita")
		{
			if ( Id == 0)
			{	
				charac.sinsVirtues.setSin(GeneralLogic.instance.storyHolder.PecadoCavaleiro);
				charac.SetMoral (GeneralLogic.instance.storyHolder.cavaleiro); 
				charac.dialogs = GeneralLogic.instance.personagem1;	
				
				setDialogs(GameDialog.Characters.Cavaleiro, charac.dialogs);
				
				
				
			}
			else if (Id == 1)
			{
				
				charac.sinsVirtues.setSin(GeneralLogic.instance.storyHolder.PecadoBobo);
				charac.SetMoral (GeneralLogic.instance.storyHolder.bobo); 
				charac.dialogs = GeneralLogic.instance.personagem2;
				
				setDialogs(GameDialog.Characters.Bobo, charac.dialogs);
			}
			else if (Id == 3)
			{
				
				charac.sinsVirtues.setSin(GeneralLogic.instance.storyHolder.PecadoIdiota);
				charac.SetMoral (GeneralLogic.instance.storyHolder.idiota); 
				charac.dialogs = GeneralLogic.instance.personagem3;
				
				setDialogs(GameDialog.Characters.Idiota, charac.dialogs);
			}
			else if (Id ==2)
			{
				
				charac.sinsVirtues.setSin(GeneralLogic.instance.storyHolder.PecadoPrincesa);
				charac.SetMoral (GeneralLogic.instance.storyHolder.princesa); 
				charac.dialogs = GeneralLogic.instance.personagem4;
				
				setDialogs(GameDialog.Characters.Princesa, charac.dialogs);
			}
			charac.materials = this.materials;
			
			
		}
		
		StopCoroutine("Entry");
	}
	
	void setDialogs(GameDialog.Characters charac, DialogStructure structure)
	{
//		bool setted = false;
		int max = GeneralLogic.instance.storyHolder.GameDialogs.Count;
		
		
		//Debug.Log (GeneralLogic.instance.storyHolder.GameDialogs.Count);
		for ( int i = 0; i < max; i++)
		{
			
			
			if ( GeneralLogic.instance.storyHolder.GameDialogs[i].Personagem == charac)
			{
			
				structure.neutralDialogsNeutralJoker 	= GeneralLogic.instance.storyHolder.GameDialogs[i].Joker_RespostaNeutra;
				structure.neutralDialogsNeutralMurder	= GeneralLogic.instance.storyHolder.GameDialogs[i].Assassino_RespostaNeutra;
				structure.neutralDialogsNeutralSuspect 	= GeneralLogic.instance.storyHolder.GameDialogs[i].Neutro_RespostaNeutra;
				
				structure.angryDialogsJoker 			= GeneralLogic.instance.storyHolder.GameDialogs[i].Joker_RespostaIrritada;
				structure.angryDialogsMurder 			= GeneralLogic.instance.storyHolder.GameDialogs[i].Assassino_RespostaIrritada;
				structure.angryDialogsSuspect 			= GeneralLogic.instance.storyHolder.GameDialogs[i].Neutro_RespostaIrritada;
				
				
				structure.goodDialogsJoker 				= GeneralLogic.instance.storyHolder.GameDialogs[i].Joker_RespostaPositiva;
				structure.goodDialogsMurder 			= GeneralLogic.instance.storyHolder.GameDialogs[i].Assassino_RespostaPositiva;
				structure.goodDialogsSuspect 			= GeneralLogic.instance.storyHolder.GameDialogs[i].Neutro_RespostaPositiva;
						
				i = max;
			} 
			
		}
		
		
	}
	
	
}
