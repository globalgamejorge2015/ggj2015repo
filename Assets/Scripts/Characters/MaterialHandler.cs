﻿using UnityEngine;
using System.Collections;

public class MaterialHandler {


	GameObject targetGo;
	Material[] materials;
	Material currentMaterial;

	// Use this for initialization
	public  MaterialHandler (GameObject go, Material[] materials) {
	
		targetGo = go;
		this.materials = materials;
		currentMaterial = targetGo.GetComponent<SpriteRenderer>().material;
		
		
	}
	
	// 0 dark
	// 1 light
	public void changeMaterial(int index)
	{
	
	}
	
	//1 light
	// 0 dark
	public void changeColor(int index)
	{
	
		if ( currentMaterial == null)
			currentMaterial = targetGo.GetComponent<SpriteRenderer>().material;
		
		Debug.Log (currentMaterial.color);
		
		if ( index == 0)
		{
			currentMaterial.color = new Color (0.7f,0.7f,0.7f,1);
			Debug.Log (currentMaterial);
		}
		else if ( index == 1)
		{
			currentMaterial.color = new Color (1,1,1,1);
		}
		
	}
	
}
