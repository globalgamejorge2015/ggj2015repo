﻿using UnityEngine;
using System.Collections;

public class SendCharacOffFloor : MonoBehaviour {

	
	public void getOutElevator()
	{
		if (!GeneralLogic.ALLINTERATIONSLOCKED)
		{
			if (GeneralLogic.instance.selectedPlayer != null && GeneralLogic.instance.selectedPlayer.name != "Ascensorita" && Elevator.MAYCHANGEFLOOR)     
			{
				
				GeneralLogic.instance.selectedPlayer.GetComponent<Sinners>().GetOffElevator();
			}
		}
	}
	
}
