﻿using UnityEngine;
using System.Collections;


[ExecuteInEditMode]
public class GetObjAtributes : MonoBehaviour {

	// Use this for initialization
	
	UiSizeAndPositionControl uiControl;
	
	void Start () {
	
		setUiControlAndDefaultPositioning();
		
	}
	
	// Update is called once per frame
	void Update () {
	
		setUiControlAndDefaultPositioning();
	
	}
	
	void setUiControlAndDefaultPositioning()
	{
		if (uiControl == null)
		{
			uiControl = GetComponent<UiSizeAndPositionControl>();
		}
		else
		{
			uiControl.setProjectedPos(transform.position.x, transform.position.y);
		}
	}
	
}
