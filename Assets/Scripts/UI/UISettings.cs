﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UISettings : MonoBehaviour {


	RectTransform cachedTransform;
	public GameObject referenceObject; 
	Vector3 vectorScale =  new Vector3(1.41f,0.77f, 1.41f );
	public Canvas canvas;
	public Vector3 offsetBox = new Vector3(0,0.8f,0); 
	
	//public float teste = 6;
	Image img;
	public Text text;
	// Use this for initialization
	void Start () {
	
		img = GetComponent<Image>();
		img.enabled = false;
		text.enabled = false;
		canvas = (Canvas) GameObject.FindWithTag("Canvas").GetComponent<Canvas>() as Canvas	;
		cachedTransform = GetComponent<RectTransform>();
		
		cachedTransform.SetParent(canvas.GetComponent<RectTransform>());
	}
	
	// Update is called once per frame
	void Update () {
	
	// calculo feito com base na distancia da camera
	// 0.8 eh a proporcao da diferença entre a distancia da camera e o tamanho legivel da caixa de texto
	// 0.16 eh a proporcao da diferenca da distance da caixa de texto legivel para o personagem em relacao ao tamanho da caixa
		offsetBox = new Vector3(0, 0.6f + 0.16f *CameraScript.instance.camera.orthographicSize  ,0);
		cachedTransform.position = CameraScript.instance.camera.WorldToScreenPoint(referenceObject.transform.position + offsetBox);
		cachedTransform.localScale = ( ((vectorScale) / (CameraScript.instance.camera.orthographicSize/CameraScript.instance.camera.orthographicSize * 0.8f) ) );
		
		
		
	}
}
	