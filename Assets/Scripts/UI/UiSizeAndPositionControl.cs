﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class UiSizeAndPositionControl : MonoBehaviour {
	
	int screenWidth ;
	int screenHeight ;
	
	float screenProportionX;
	float screenProportionY;
	
	float projectedScreenWidth = 1203;
	float projectedScreenHeight = 677 ;
	
	float posX ;
	float posY ;
	
	float projectedPosX;
	float projectedPosY;
	
	Transform cachedTransform;
	public Canvas canvasRef;
	CanvasScaler scalerRef;
	
	
	// Use this for initialization
	void Start () {
		
		scalerRef = canvasRef.GetComponent<CanvasScaler>();
		
		cachedTransform = transform;
		
		setPos(transform.position.x, transform.position.y);
		setResolutionVariables(Screen.width, Screen.height);
		setScreenProportion();
		changeUiSize();
	}
	
	
	void setPos(float x, float y)
	{
		// set actual pos
		posX = x;
		posY = y;
		
	}
	
	void setScreenProportion()
	{
		
		screenProportionX = projectedScreenWidth/screenWidth;
		screenProportionY = projectedScreenHeight/screenHeight;
		
		
	}
	
	public void setProjectedPos(float x, float y)
	{
	
		// set the project X and Y positioning
		projectedPosX = x;
		projectedPosY = y;
		
		//Debug.Log (x);
		//Debug.Log (y);
		
	}
	
	public void setResolutionVariables(int x, int y)
	{
		// set internal control vars
		
		screenWidth  = x;
		screenHeight = y;
	}
	
	
	void changeUiSize()
	{
		
		//cachedTransform.localScale = new Vector3(1/screenProportionX , 1/screenProportionY, 1);
		scalerRef.scaleFactor = scalerRef.scaleFactor/screenProportionX;
	}
	
	// Update is called once per frame
	void Update () {
		
		
		
	}
	
	
}
