﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ReferenceToUIRecord : MonoBehaviour {

	public Image img;
	public Text text;
	public static ReferenceToUIRecord instance;

	// Use this for initialization
	void Start () {
		
		instance = this;
		
	}
	
	// Update is called once per frame
	
	public void changeProfileSettings(Sprite sprite, string texString )
	{
		
		img.sprite = sprite;
		text.text = texString;
		
		
	}
	
	
}
